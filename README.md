
# GPGPU_SHA

**Original Project Description**

```
This is a project I did for algorithms class in college. It is an
implementation of SHA1 and PARSHA-256 algorithms on GPU. I believe that
PARSHA-256 has some bugs due to byte ordering.
```

**New Project Goals**

This project is a fork of the original repo. The goal now is to go through
the original source code and make optimizations in the code to maximize the
runtime of execution.

We are making these changes based off of documents from our _Structured Comp
Org_ class with some guidance from the teacher and LA's.

## Results

The full report can be found in the `docs/optimizations` directory of this repo.
The other directory under `docs` contains the original documents of the repo.

## Compilation

Run `make parsha256test` from the `src` directory.

## Execution

| Executable | Description | Compileable |
| --- | --- | --- |
| `./sha1test` | SHA-1 performance test | No |
| `./parsha256test` | PARSHA-256 performance test on GPU | No |
| `./parsha256testemu` | PARSHA-256 performance test on CPU (emulation mode) | No |

## Contact

| Member | E-Mail |
| --- | --- |
| Manuel de Armas | mdear026@fiu.edu |
| Daniel A. Herrera | dherr060@fiu.edu |
| Carlos Larrauri | clarr028@fiu.edu |
| Alejandro Mangano | amang011@fiu.edu |

